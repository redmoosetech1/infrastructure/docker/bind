FROM debian:stable-slim

RUN cat /etc/apt/sources.list

# Change Debian Sources List
RUN sed -i -e '/^#/d' /etc/apt/sources.list  && \
    sed -i -e 's/deb.debian.org/mirror.rmt/g' \
    -e 's/security.debian.org/mirror.rmt/g' \
    -e 's/snapshot.debian.org/mirror.rmt/g' \
    /etc/apt/sources.list

# Install Base Packages
RUN DEBIAN_FRONTEND=non-interactive apt-get update -qq -y && \
    DEBIAN_FRONTEND=non-interactive apt-get install -qq -y --no-install-recommends \
    bind9 \
    bind9utils \
    stubby \
    dnsutils \
    ca-certificates \
    dns-root-data \
    ldnsutils \
    libyaml-0-2 \
    libssl-dev \
    gnupg \
    dirmngr \
    iputils-ping

# Add Internal Root CA
ADD https://mirror.rmt/certificates/rmt.crt /usr/local/share/ca-certificates/rmt.crt
RUN update-ca-certificates

EXPOSE 8053/tcp \
       8053/udp \
       53/tcp \
       53/udp

